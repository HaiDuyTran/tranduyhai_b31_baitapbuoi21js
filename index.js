//Tạo ra mảng lưu trữ nhiều nhân viên
var mangNhanVien = [];

// Lấy localstorage lưu vào mảng nhân viên
var dataJson = localStorage.getItem("DSNV");
if (dataJson !== null) {
  //   dssv = JSON.parse(dataJson).map();
  // array sau khi lấy lên từ localStorage
  // oject khi lưu xuống localStorage sẽ bị mất method (function)
  var arrayDSNV = JSON.parse(dataJson);

  for (var index = 0; index < arrayDSNV.length; index++) {
    var item = arrayDSNV[index];
    var nv = new NhanVien(
      item.taiKhoan,
      item.hoTen,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );

    mangNhanVien.push(nv);
  }
  renderMangNhanVien(mangNhanVien);
}
// hàm lưu dssv vào LocalStorage
var luuLocalStorage = function (dsnv) {
  var dsnvJSON = JSON.stringify(dsnv);

  localStorage.setItem("DSNV", dsnvJSON);
};

/**
 * hàm render ra màn hình
 */
function renderMangNhanVien(arrNhanVien) {
  var contentHTML = "";
  for (var i = 0; i < arrNhanVien.length; i++) {
    var nVien = arrNhanVien[i];
    var contentTR = `<tr>
        <td>${nVien.taiKhoan}</td>
        <td>${nVien.hoTen}</td>
        <td>${nVien.email}</td>
        <td>${nVien.ngayLam}</td>
        <td>${nVien.chucVu}</td>
        <td>${nVien.tongLuong()}</td>
        <td>${nVien.xepLoai()}</td>
        <td><button class="btn btn-danger" onclick="xoaNhanVien(${
          nVien.taiKhoan
        })">Xóa</button>
        <button class="btn btn-primary" onclick="layThongTinSinhVien(${
          nVien.taiKhoan
        })" id="btnThem" data-toggle="modal" data-target="#myModal">
         Sửa
        </button>
        </td>
      
      </tr>`;
    contentHTML = contentHTML + contentTR;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function themNV() {
  var nVien = layThongTinTuForm();

  //validation
  var valid = true;
  // Kiểm tra rỗng
  valid &=
    kiemTraRong(nVien.taiKhoan, "#taiKhoanRongError", "Tài khoản") &
    kiemTraRong(nVien.hoTen, "#tenNhanVienRongError", "Tên nhân viên") &
    kiemTraRong(nVien.email, "#emailRongError", "Email");
  // Kiểm tra độ dài
  valid &=
    kiemTraDoDai(nVien.taiKhoan, "#taiKhoanDoDaiError", "Tài khoản", 4, 6) &
    kiemTraDoDai(nVien.matKhau, "#matKhauDoDaiError", "Mật khẩu", 6, 10);
  // Kiểm tra chữ
  valid &= kiemTraTatCaCacKiTu(nVien.hoTen, "#tenNhanVienTypeError", "Họ tên");
  // Kiểm tra số
  valid &=
    kiemTraSo(nVien.luongCB, "#luongCBNumberError", "Lương cơ bản") &
    kiemTraSo(nVien.gioLam, "#gioLamNumberError", "Giờ làm");
  // Kiểm tra email
  valid &= kiemTraEmail(nVien.email, "#emailTypeError", "Email");
  // Kiểm tra giá trị
  valid &=
    kiemTraGiaTri(
      nVien.luongCB,
      "#luongCBValueError",
      "Lương cơ bản",
      1000000,
      20000000
    ) & kiemTraGiaTri(nVien.gioLam, "#gioLamValueError", "Giờ làm", 80, 200);
  // Kiểm tra date
  valid &= kiemTraDate(nVien.ngayLam, "#ngayLamTypeError", "Ngày làm");
  // Kiểm tra chức vụ
  valid &= kiemTraChucVu(nVien.chucVu, "#chucVuError", "Chức vụ ");
  if (!valid) {
    return;
  }
  var isValid = kiemTraNhanVien(mangNhanVien, nVien.taiKhoan);

  if (isValid) {
    mangNhanVien.push(nVien);

    renderMangNhanVien(mangNhanVien);
    luuLocalStorage(mangNhanVien);
  }
}

function xoaNhanVien(taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);

  mangNhanVien.splice(viTri, 1);
  renderMangNhanVien(mangNhanVien);
  luuLocalStorage(mangNhanVien);
}

function kiemTraNhanVien(mangNhanVien, taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);
  if (viTri == -1) {
    document.getElementById("kiemTraThongTin").innerText = "";

    return true;
  } else {
    document.getElementById("kiemTraThongTin").innerText = "Tài khoản bị trùng";
    return false;
  }
}
function layThongTinSinhVien(taiKhoan) {
  var viTri = timKiemViTri(mangNhanVien, taiKhoan);

  if (viTri == -1) {
    // nếu không tìm thấy thì dừng hàm
    return;
  }

  var nhanVien = mangNhanVien[viTri];
  document.getElementById("tknv").disabled = true;
  hienThiThongTinLenForm(nhanVien);
}

function capNhatNhanVien() {
  var nVien = layThongTinTuForm();

  var viTri = timKiemViTri(mangNhanVien, nVien.taiKhoan);

  mangNhanVien[viTri] = layThongTinTuForm();
  //validation
  var valid = true;
  // Kiểm tra rỗng
  valid &=
    kiemTraRong(nVien.taiKhoan, "#taiKhoanRongError", "Tài khoản") &
    kiemTraRong(nVien.hoTen, "#tenNhanVienRongError", "Tên nhân viên") &
    kiemTraRong(nVien.email, "#emailRongError", "Email");
  // Kiểm tra độ dài
  valid &=
    kiemTraDoDai(nVien.taiKhoan, "#taiKhoanDoDaiError", "Tài khoản", 4, 6) &
    kiemTraDoDai(nVien.matKhau, "#matKhauDoDaiError", "Mật khẩu", 6, 10);
  // Kiểm tra chữ
  valid &= kiemTraTatCaCacKiTu(nVien.hoTen, "#tenNhanVienTypeError", "Họ tên");
  // Kiểm tra số
  valid &=
    kiemTraSo(nVien.luongCB, "#luongCBNumberError", "Lương cơ bản") &
    kiemTraSo(nVien.gioLam, "#gioLamNumberError", "Giờ làm");
  // Kiểm tra email
  valid &= kiemTraEmail(nVien.email, "#emailTypeError", "Email");
  // Kiểm tra giá trị
  valid &=
    kiemTraGiaTri(
      nVien.luongCB,
      "#luongCBValueError",
      "Lương cơ bản",
      1000000,
      20000000
    ) & kiemTraGiaTri(nVien.gioLam, "#gioLamValueError", "Giờ làm", 80, 200);
  // Kiểm tra date
  valid &= kiemTraDate(nVien.ngayLam, "#ngayLamTypeError", "Ngày làm");
  // Kiểm tra chức vụ
  valid &= kiemTraChucVu(nVien.chucVu, "#chucVuError", "Chức vụ ");
  if (!valid) {
    return;
  }
  document.getElementById("tknv").disabled = false;
  renderMangNhanVien(mangNhanVien);
  luuLocalStorage(mangNhanVien);
}
function timKiemNhanVien() {
  var mangViTri = [];
  for (var i = 0; i < mangNhanVien.length; i++) {
    var item = mangNhanVien[i];
    if (item.xepLoai() == document.getElementById("searchName").value) {
      mangViTri.push(mangNhanVien[i]);
    } else if (
      document.getElementById("searchName").value == "Tất cả các nhân viên"
    ) {
      mangViTri.push(item);
    }
  }
  renderMangNhanVien(mangViTri);
}

// function timKiemNhanVien(mangNhanVien) {
//   var xepLoai = document.getElementById("searchName").value;
//
//   if (xepLoai == "Loại nhân viên") {
//     document.getElementById("loaiNhanVienError").innerHTML =
//       "Loại nhân viên không hợp lệ";
//   }else {
//     document.getElementById("loaiNhanVienError").innerHTML =
//       "";
//   }
//   timKiemNhanVien2(mangNhanVien,taiKhoan);
// }
