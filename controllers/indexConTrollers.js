function timKiemViTri(mangNhanVien, taiKhoan) {
  return mangNhanVien.findIndex(function (item) {
    return item.taiKhoan == taiKhoan;
  });
}


var layThongTinTuForm = function () {
  var taiKhoan = document.getElementById("tknv").value;
  var hoTen = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value * 1;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value * 1;
  var nVien = new NhanVien(
    taiKhoan,
    hoTen,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
  return nVien;
};

var hienThiThongTinLenForm = function (nVien) {
  document.getElementById("tknv").value = nVien.taiKhoan;
  document.getElementById("name").value = nVien.hoTen;
  document.getElementById("email").value = nVien.email;
  document.getElementById("password").value = nVien.matKhau;
  document.getElementById("datepicker").value = nVien.ngayLam;
  document.getElementById("luongCB").value = nVien.luongCB;
  document.getElementById("chucvu").value = nVien.chucVu;
  document.getElementById("gioLam").value = nVien.gioLam;
};
