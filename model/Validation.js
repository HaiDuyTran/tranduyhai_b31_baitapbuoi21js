function kiemTraRong(value, selectorError, name) {
  if (value == "") {
    document.querySelector(selectorError).innerHTML =
      name + ` không được bỏ trống!`;
    return false;
  }
  document.querySelector(selectorError).innerHTML = ``;
  return true;
}

function kiemTraDoDai(value, selectorError, name, minLength, maxLength) {
  if (value.length < minLength || value.length > maxLength) {
    document.querySelector(selectorError).innerHTML =
      name + " từ " + minLength + " đến " + maxLength + " ký tự ";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraTatCaCacKiTu(value, selectorError, name) {
  var regexLetter = /^[A-Z a-z]+$/; // Nhập các kí tự a->z A->Z hoặc khoảng trống không bao gồm unicode
  if (regexLetter.test(value)) {
    // test nếu ok
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " phải là chữ cái! ";
  return false;
}

function kiemTraSo(value, selectorError, name) {
  var regexNumber = /^[0-9]+$/;
  if (regexNumber.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML = name + " tất cả là số";
  return false;
}

function kiemTraEmail(value, selectorError, name) {
  var regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " không đúng định dạng";
  return false;
}

function kiemTraGiaTri(value, selectorError, name, minValue, maxValue) {
  if (Number(value) < minValue || Number(value) > maxValue) {
    document.querySelector(selectorError).innerHTML =
      name + " giá trị từ " + minValue + " đến " + maxValue;
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
function kiemTraDate(value, selectorError, name) {
  var regexDate = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
  if (regexDate.test(value)) {
    document.querySelector(selectorError).innerHTML = "";
    return true;
  }
  document.querySelector(selectorError).innerHTML =
    name + " không đúng định dạng";
  return false;
}
function kiemTraChucVu(value, selectorError, name) {
  if (value == "Chọn chức vụ") {
    document.querySelector(selectorError).innerHTML = name + " không hợp lệ";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}

function kiemTraLoaiNhanVien(value, selectorError, name) {
  if (value == "Loại nhân viên") {
    document.querySelector(selectorError).innerHTML = name + " không hợp lệ";
    return false;
  }
  document.querySelector(selectorError).innerHTML = "";
  return true;
}
